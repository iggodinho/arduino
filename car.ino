const int engine1L=6,engine2L=5,engine1R=3,engine2R=2;
int state_temp,vSpeed = 200,vSpeedL=vSpeed/2;
char state;

void setup() {
  
  pinMode(engine1L,OUTPUT); //engine1L: Left first engine
  pinMode(engine2L, OUTPUT); //engine2L: Left second engine
  pinMode(engine1R, OUTPUT); //engine1R: Right first engine
  pinMode(engine2R, OUTPUT); //engine2R: Right second engine
  digitalWrite(engine1L, LOW);
  digitalWrite(engine2L, LOW);
  digitalWrite(engine1R, LOW);
  digitalWrite(engine2R, LOW);
  Serial.begin(9600);
}
 
void loop() {
  
  if (Serial.available() > 0) {
    state_temp = Serial.read();
    state = state_temp;
  }
  if (state == '8') {
    Serial.println("Comando para Frente");
    digitalWrite(engine1L, vSpeed);
    digitalWrite(engine2L, LOW);
    digitalWrite(engine1R, vSpeed);
    digitalWrite(engine2R, LOW);
  }
  else if (state == '7') {  
      Serial.println("Comando para Frente-Esquerda");
      digitalWrite(engine1L, vSpeedL);
      digitalWrite(engine2L, LOW);
      digitalWrite(engine1R, vSpeed);
      digitalWrite(engine2R, LOW);
      state = '8';
  }
  else if (state == '9') {   
    Serial.println("Comando para Frente-Direita");
      digitalWrite(engine1L, vSpeed);
      digitalWrite(engine2L, LOW);
      digitalWrite(engine1R, vSpeedL);
      digitalWrite(engine2R, LOW);
      state = '8';
  }
  else if (state == '2') { 
    Serial.println("Comando para Trás");
      digitalWrite(engine1L, LOW);
      digitalWrite(engine2L, vSpeed);
      digitalWrite(engine1R, LOW);
      digitalWrite(engine2R, vSpeed);
  }
  else if (state == '1') {  
    Serial.println("Comando para Trás-Esquerda");
      digitalWrite(engine1L, LOW);
      digitalWrite(engine2L, vSpeedL);
      digitalWrite(engine1R, LOW);
      digitalWrite(engine2R, vSpeed);
      state = '2';
  }
  else if (state == '3') {  
    Serial.println("Comando para Trás-Direita");
      digitalWrite(engine1L, LOW);
      digitalWrite(engine2L, vSpeed);
      digitalWrite(engine1R, LOW);
      digitalWrite(engine2R, vSpeedL);
      state = '2';
  }
  else if (state == '4') {   
    Serial.println("Comando para Esquerda");
      digitalWrite(engine1L, LOW);
      digitalWrite(engine2L, vSpeed);
      digitalWrite(engine1R, vSpeed);
      digitalWrite(engine2R, LOW);
      state = '8';
  }
  else if (state == '6') {   
    Serial.println("Comando para Direita");
      digitalWrite(engine1L, vSpeed);
      digitalWrite(engine2L, LOW);
      digitalWrite(engine1R, LOW);
      digitalWrite(engine2R, vSpeed);
  }
  else if (state == '5') {   
    Serial.println("Comando para Parar");
      digitalWrite(engine1L, LOW);
      digitalWrite(engine2L, LOW);
      digitalWrite(engine1R, LOW);
      digitalWrite(engine2R, LOW);
  }
}


